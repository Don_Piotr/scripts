#!/bin/bash

xml_url="www.bing.com/HPImageArchive.aspx?format=xml&idx=0&n=1&mkt=it-IT"
base_url=$(echo $(wget -qO - $xml_url) | grep -oP "<urlBase>(.*)</urlBase>" | cut -d">" -f2 | cut -d"<" -f1)
pic_url="www.bing.com"$base_url"_1366x768.jpg"
wget -O ~/img/bing_wallpaper.jpg $pic_url
convert ~/img/bing_wallpaper.jpg ~/img/bing_wallpaper.png

